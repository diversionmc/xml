plugins {
    id("java-library")
    id("net.thebugmc.gradle.sonatype-central-portal-publisher") version "1.2.3"
    id("net.thebugmc.gradle.javadoc-tester") version "1.0.0"
}

description = "XML implementation using Parser API."
group = "net.thebugmc"
version = "1.2.0"

java.toolchain.languageVersion = JavaLanguageVersion.of(21)

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    api("net.thebugmc:parser:1.1.1")
    api("net.thebugmc:result:2.0.0")

    testImplementation("org.junit.jupiter:junit-jupiter:5.9.2")
    testRuntimeOnly("org.junit.platform:junit-platform-launcher:1.9.2")
}

centralPortal {
    pom {
        url = "https://gitlab.com/thebugmc/result"
        licenses {
            license {
                name = "BSD-2-Clause"
                url = "https://opensource.org/licenses/BSD-2-Clause"
            }
        }
        developers {
            developer {
                name = "Kirill Semyonkin"
                email = "burnytc@gmail.com"
                organization = "The Bug Network"
                organizationUrl = "https://thebugmc.net/"
            }
        }
        scm {
            connection = "scm:git:git://gitlab.com/thebugmc/promise.git"
            developerConnection = "scm:git:ssh://git@gitlab.com:thebugmc/promise.git"
            url = "https://gitlab.com/thebugmc/promise/tree/master"
        }
    }
}

signing {
    useGpgCmd()
}

tasks {
    test {
        useJUnitPlatform()
        testLogging {
            events("passed", "skipped", "failed", "standardOut", "standardError")
        }
    }
}
