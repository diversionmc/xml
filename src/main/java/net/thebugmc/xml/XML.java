package net.thebugmc.xml;

import net.thebugmc.error.Result;
import net.thebugmc.error.TryS;
import net.thebugmc.parser.Parser;
import net.thebugmc.parser.expression.*;
import net.thebugmc.parser.pattern.PatternResult;
import net.thebugmc.parser.pattern.Sentence;
import net.thebugmc.parser.util.FilePointer;

import java.io.File;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

import static java.lang.Character.isWhitespace;
import static java.util.Collections.unmodifiableList;
import static java.util.Collections.unmodifiableMap;
import static java.util.Map.entry;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Stream.concat;
import static net.thebugmc.parser.expression.PieceResult.*;
import static net.thebugmc.parser.pattern.ParsePattern.match;
import static net.thebugmc.parser.util.ParserException.ASSERT;

/**
 * XML parses Extensible Markup Language data into this class,
 * which represents either a tag node (a list of children) or a plain text node.
 */
public class XML extends Sentence {
    private XML(FilePointer ptr, String tagName, Map<String, String> attributes, List<XML> children) {
        super(ptr);
        this.tagName = tagName;
        this.attributes = attributes == null ? null : unmodifiableMap(attributes);
        this.children = children == null ? null : unmodifiableList(children);
    }

    /**
     * Form an inline representation of this XML node.
     */
    public String toString() {
        if (children == null) return tagName; // text
        if (tagName.isEmpty()) // root
            return children.stream()
                .map(XML::toString)
                .collect(joining());
        return "<"                                                           // <
            + concat(Stream.of(entry(tagName, "")), attributes.entrySet().stream()) // tag attributes
            .map(a -> a.getValue().isEmpty()
                ? a.getKey()
                : a.getKey() + "=\"" + Text.escape(a.getValue()) + "\"")     // attribute(="value")
            .collect(joining(" "))                                           // tag attr attr...
            + (children.isEmpty()
            ? " />"                                                          // />
            : (">" + children.stream()                                       // >...
            .map(XML::toString)
            .collect(joining())
            + "</" + tagName + ">"));                                        // </tag>
    }

    //
    // Tag
    //

    private final String tagName;

    /**
     * Get the name of this tag. Empty for document root node.
     *
     * @return Tag name if this node is a tag XML node, text content otherwise.
     */
    public String tagName() {
        return tagName;
    }

    /**
     * @deprecated Use {@link #tag()} (DN naming convention)
     */
    public boolean isTag() {
        return tag();
    }

    /**
     * Check if this XML node is a tag node.
     *
     * @return True if this is a tag XML node.
     */
    public boolean tag() {
        return children != null;
    }

    //
    // Attributes
    //

    private final Map<String, String> attributes;

    /**
     * Get attributes of this XML node.
     *
     * @return Unmodifiable map of attributes if this node is a tag XML node, null otherwise.
     */
    public Map<String, String> attributes() {
        return attributes;
    }

    /**
     * Look for an attribute with given key.
     *
     * @param key Key of the attribute to look for.
     * @return Optional of attribute value. May be empty string for attributes that do not have their value defined.
     */
    public Optional<String> findAttribute(String key) {
        return ofNullable(attributes.get(key));
    }

    //
    // Children
    //

    private final List<XML> children;

    /**
     * Get amount of children in this list.
     *
     * @return Children amount, 0 if not a tag.
     */
    public int size() {
        return children == null ? 0 : children.size();
    }

    /**
     * @deprecated Use {@link #toList()} (Stream API Parity)
     */
    @Deprecated(since = "1.1.0")
    public List<XML> children() {
        return toList();
    }

    /**
     * Get shallow list of children of this XML node.
     *
     * @return Shallow unmodifiable list of children if this node is a tag XML node, null otherwise.
     */
    public List<XML> toList() {
        return children;
    }

    /**
     * @deprecated Use {@link #stream()} (Stream API Parity)
     */
    @Deprecated(since = "1.1.0")
    public Stream<XML> childStream() {
        return stream();
    }

    /**
     * Get shallow stream of children of this XML node.
     *
     * @return Shallow stream of child nodes.
     * @throws NullPointerException If current node is a text XML node.
     */
    public Stream<XML> stream() {
        return children.stream();
    }

    /**
     * @deprecated Use {@link #filter(String)} (Stream API Parity)
     */
    @Deprecated(since = "1.1.0")
    public Stream<XML> filterChildren(String tag) {
        return filter(tag);
    }

    /**
     * Get stream of all shallow children tags filtered by tag name.
     *
     * @param tag Tag name.
     * @return Shallow stream of tag children matching tag name.
     */
    public Stream<XML> filter(String tag) {
        return stream()
            .filter(XML::tag)
            .filter(c -> c.tagName.equals(tag));
    }

    /**
     * @deprecated Use {@link #find(String...)} (Stream API Parity)
     */
    @Deprecated(since = "1.1.0")
    public Optional<XML> findChild(String... tag) {
        return find(tag);
    }

    /**
     * Find first tag child in the XML tree with root at this node.
     *
     * @param tag Tag path to look for. Empty path gives optional of this XML.
     * @return Optional of child found.
     */
    public Optional<XML> find(String... tag) {
        var opt = Optional.of(this);
        for (var t : tag)
            opt = opt
                .flatMap(c -> c
                    .filter(t)
                    .findFirst());
        return opt;
    }

    //
    // Text
    //

    /**
     * @deprecated Use {@link #textContent()} (HTML parity)
     */
    @Deprecated(since = "1.1.0")
    public String asText() {
        return textContent();
    }

    /**
     * Convert this XML into plain text.
     *
     * @return Content for text XML nodes and conjoined children also converted to plain text for tag XML nodes.
     */
    public String textContent() {
        return children == null
            ? tagName
            : children.stream()
            .map(XML::asText)
            .collect(joining());
    }

    /**
     * @deprecated Use {@link #text()} (DN naming convention)
     */
    public boolean isText() {
        return text();
    }

    /**
     * Check if this XML node is a text node.
     *
     * @return True if this is a text XML node.
     */
    public boolean text() {
        return children == null;
    }

    //
    // XML Parse
    //

    private static class Tag extends ExpressionPiece {
        private StringBuilder tagName;
        private StringBuilder attributes;
        private boolean start = false;
        private boolean closing = false;

        public Tag(FilePointer ptr) {
            super(ptr);
        }

        public PieceResult read(char c, FilePointer ptr) {
            if (!start) { // <
                start = true;
                return CONTINUE;
            }
            if (c == '>') return REPLACE_TAKE;

            if (isWhitespace(c)) {
                if (tagName != null
                    && attributes == null) {
                    ASSERT(!closing, ptr, "Invalid XML tag (</tag ...>)");
                    attributes = new StringBuilder();
                }
                return CONTINUE;
            }
            if (attributes != null) attributes.append(c);
            else if (tagName != null) tagName.append(c);
            else if (c == '/') {
                ASSERT(!closing, ptr, "Invalid XML tag (<// start)");
                closing = true;
            } else tagName = new StringBuilder().append(c);
            return CONTINUE;
        }

        public ExpressionPiece replace(FilePointer ptr) {
            ASSERT(tagName != null && !tagName.isEmpty(), ptr, "Invalid XML tag (<>)");
            var tag = tagName + "";
            if (tag.equals("?xml")) return null;

            if (closing) return new CloseTag(pointer(), tag);

            var attr = attributes == null ? "" : (attributes + "").trim();
            int i = attr.lastIndexOf('/');
            if (i >= 0 && i == attr.length() - 1) // empty content (<tag attributes />)
                return new GroupTag(pointer(), List.of(), tag, parseAttributes(attr.substring(0, i)));

            return new OpenTag(pointer(), tag, parseAttributes(attr));
        }
    }

    private static class GroupTag extends GroupPiece {
        private final String tagName;
        private final Map<String, String> attributes;

        public GroupTag(FilePointer ptr, List<ExpressionPiece> content, String tagName, Map<String, String> attributes) {
            super(ptr, content);
            this.tagName = tagName;
            this.attributes = attributes;
        }

        public String tagName() {
            return tagName;
        }

        public Map<String, String> attributes() {
            return attributes;
        }

        public String toString() {
            return "<" + tagName + (attributes.isEmpty() ? "" : " ...") + ">" + content() + "</" + tagName + ">";
        }
    }

    private static class CloseTag extends ExpressionPiece {
        private final String tagName;

        public CloseTag(FilePointer ptr, String tagName) {
            super(ptr);
            this.tagName = tagName;
        }

        public PieceResult read(char c, FilePointer ptr) {
            return LEAVE;
        }

        public String tagName() {
            return tagName;
        }

        public String toString() {
            return "</" + tagName + ">";
        }
    }

    private static class OpenTag extends ExpressionPiece {
        private final String tagName;
        private final Map<String, String> attributes;

        public OpenTag(FilePointer ptr, String tagName, Map<String, String> attributes) {
            super(ptr);
            this.tagName = tagName;
            this.attributes = attributes;
        }

        public PieceResult read(char c, FilePointer ptr) {
            return LEAVE;
        }

        public String tagName() {
            return tagName;
        }

        public Map<String, String> attributes() {
            return attributes;
        }

        public String toString() {
            return "<" + tagName + (attributes.isEmpty() ? "" : " ...") + ">";
        }
    }

    private static class Text extends ExpressionPiece {
        private final StringBuilder content = new StringBuilder();

        public Text(FilePointer ptr) {
            super(ptr);
        }

        public PieceResult read(char c, FilePointer ptr) {
            if (c == '\r') return CONTINUE;
            if (c == '<') return LEAVE;
            content.append(c == '\n' ? ' ' : c);
            return CONTINUE;
        }

        public String toString() {
            return content + "";
        }

        public String textContent() {
            return unescape(this + "");
        }

        public static String unescape(String text) {
            var res = new StringBuilder();
            StringBuilder entity = null;
            for (int i = 0; i < text.length(); i++) {
                char c = text.charAt(i);
                if (entity != null) {
                    if (c == ';') {
                        res.append(
                            switch (entity + "") {
                                case "amp" -> '&';
                                case "gt" -> '>';
                                case "lt" -> '<';
                                case "apos" -> '\'';
                                case "quot" -> '"';
                                default -> throw new IllegalArgumentException("Unknown entity &" + entity + ";");
                            });
                        entity = null;
                    } else entity.append(c);
                } else if (c == '&') entity = new StringBuilder();
                else res.append(c);
            }
            if (entity != null) throw new IllegalArgumentException("Incomplete entity &" + entity);
            return res + "";
        }

        public static String escape(String text) {
            var str = new StringBuilder();
            for (int i = 0; i < text.length(); i++) {
                char c = text.charAt(i);
                str.append(
                    switch (c) {
                        case '"' -> "&quot;";
                        case '<' -> "&lt;";
                        case '>' -> "&gt;";
                        case '\'' -> "&apos;";
                        case '&' -> "&amp;";
                        default -> c;
                    });
            }
            return str + "";
        }
    }

    /**
     * Parse an XML file.
     *
     * @param xml XML file to parse.
     * @return Parsed XML root.
     * @throws IOError If an underlying exception occurred while reading the file.
     */
    public static Result<XML, IOException> from(File xml) {
        return Result.tryGet(
            IOException.class,
            (TryS<XML, IOException>)
                () -> parse(new Parser<XML>().readFrom(xml)));
    }

    /**
     * Parse an XML document stream.
     *
     * @param xml XML document stream to parse.
     * @return Parsed XML root.
     * @throws IOError If an underlying exception occurred while reading the stream.
     */
    public static Result<XML, IOException> from(InputStream xml) {
        return Result.tryGet(
            IOException.class,
            (TryS<XML, IOException>)
                () -> parse(new Parser<XML>().readFrom(xml)));
    }

    /**
     * Parse an XML string.
     *
     * @param xml XML string to parse.
     * @return Parsed XML root.
     */
    public static Result<XML, IOException> from(String xml) {
        return Result.tryGet(
            IOException.class,
            (TryS<XML, IOException>)
                () -> parse(new Parser<XML>().text(xml)));
    }

    private static XML parse(Parser<XML> xmlp) {
        var currentTagGroupName = new AtomicReference<String>();
        return new XML(new FilePointer(xmlp.name(), 1, 1), "", Map.of(),
            xmlp.piece((c, ptr) -> c == '<', (c, ptr) -> new Tag(ptr))
                .piece((c, ptr) -> c != '<', (c, ptr) -> new Text(ptr))
                .<OpenTag, CloseTag>group(
                    l -> {
                        if (!(l instanceof OpenTag t)) return false;

                        var tag = currentTagGroupName.get();
                        if (tag != null) return tag.equals(t.tagName()); // <tag> inside <tag>, case-sensitive

                        currentTagGroupName.set(t.tagName());
                        return true;
                    },
                    r -> {
                        if (!(r instanceof CloseTag t)) return false;

                        var tag = currentTagGroupName.get();
                        if (!tag.equals(t.tagName())) return false; // </tag> must match <tag>, case-sensitive

                        currentTagGroupName.set(null);
                        return true;
                    },
                    (l, r, e) -> new GroupTag(l.pointer(), e, l.tagName(), l.attributes())) // <tag>...</tag>
                .pattern("tag", e -> { // <tag>
                    if (!(e.get(0) instanceof GroupTag t)) return null;
                    var res = match(t.content(), xmlp.patterns());
                    return new PatternResult<>(1, new XML(t.pointer(), t.tagName(), t.attributes(), res)); // isTag() -> true
                })
                .pattern("text", e -> { // <text>
                    if (!(e.get(0) instanceof Text t)) return null;
                    return new PatternResult<>(1, new XML(t.pointer(), t.textContent(), null, null)); // isText() -> true
                })
                .build());
    }

    //
    // Attribute Parse
    //

    private static class EqualAttribute extends CharPiece {
        public EqualAttribute(FilePointer ptr) {
            super(ptr, '=');
        }
    }

    private static class StringAttribute extends ExpressionPiece {
        private final StringBuilder content = new StringBuilder();
        private char start;
        private StringBuilder entity = null;

        public StringAttribute(FilePointer ptr) {
            super(ptr);
        }

        public PieceResult read(char c, FilePointer ptr) {
            if (start == 0) {
                start = c;
                return CONTINUE;
            }
            if (c == start) {
                ASSERT(entity == null, ptr, "Incomplete entity &" + entity);
                return TAKE;
            }

            if (entity != null) {
                if (c == ';') {
                    content.append(
                        switch (entity + "") {
                            case "amp" -> '&';
                            case "gt" -> '>';
                            case "lt" -> '<';
                            case "apos" -> '\'';
                            case "quot" -> '"';
                            default -> throw new IllegalArgumentException("Unknown entity &" + entity + ";");
                        });
                    entity = null;
                } else entity.append(c);
            } else if (c == '&') entity = new StringBuilder();
            else content.append(c);
            return CONTINUE;
        }

        public String content() {
            return content + "";
        }

        public String toString() {
            return "\"" + Text.escape(content()) + "\"";
        }
    }

    private static class XMLAttribute extends Sentence implements Entry<String, String> {
        private final String key;
        private final String value;

        public XMLAttribute(FilePointer ptr, String key, String value) {
            super(ptr);
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }

        public String setValue(String value) {
            throw new UnsupportedOperationException("XMLAttribute is immutable");
        }
    }

    /**
     * Parse a string of kind <code>key="value" key ...</code>.
     *
     * @param attrString String to parse.
     * @return Map of attributes from string, where values "" and non-present are represented by empty strings (not null).
     */
    public static Map<String, String> parseAttributes(String attrString) {
        var xmla = new Parser<XMLAttribute>();
        return Map.ofEntries(xmla
            .text(attrString)
            .piece((c, ptr) -> NamePiece.check(c, "-'()+,./:?;!*#@$_%"), (c, ptr) -> new NamePiece(ptr, "-'()+,./:?;!*#@$_%"))
            .piece((c, ptr) -> c == '=', (c, ptr) -> new EqualAttribute(ptr))
            .piece((c, ptr) -> c == '"' || c == '\'', (c, ptr) -> new StringAttribute(ptr))
            .pattern("attribute", e -> {
                int limit = e.size();
                if (!(e.get(0) instanceof NamePiece key)) return null;

                if (limit < 3 || !(e.get(1) instanceof EqualAttribute))
                    return new PatternResult<>(1, new XMLAttribute(key.pointer(), key.name(), ""));

                if (!(e.get(2) instanceof StringAttribute value)) return null;
                return new PatternResult<>(3, new XMLAttribute(key.pointer(), key.name(), value.content()));
            })
            .build()
            .toArray(XMLAttribute[]::new));
    }
}
